' Project:     Fake Calculator
' Start Date:  29 March 2006
' Last Update: 10 October 2007
' License:     GPL Version 3
' Description: At first glance, this appears to be a standard 4-function calculator.
'               However, there's a random number generator in it, which causes it to
'               spit out weird and completely wrong answers every now and then.
Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblDisplay As System.Windows.Forms.TextBox
    Friend WithEvents button1 As System.Windows.Forms.Button
    Friend WithEvents button2 As System.Windows.Forms.Button
    Friend WithEvents button3 As System.Windows.Forms.Button
    Friend WithEvents button4 As System.Windows.Forms.Button
    Friend WithEvents button5 As System.Windows.Forms.Button
    Friend WithEvents button6 As System.Windows.Forms.Button
    Friend WithEvents button7 As System.Windows.Forms.Button
    Friend WithEvents button8 As System.Windows.Forms.Button
    Friend WithEvents button9 As System.Windows.Forms.Button
    Friend WithEvents button0 As System.Windows.Forms.Button
    Friend WithEvents buttonEqual As System.Windows.Forms.Button
    Friend WithEvents buttonAdd As System.Windows.Forms.Button
    Friend WithEvents buttonSubtract As System.Windows.Forms.Button
    Friend WithEvents buttonMultiply As System.Windows.Forms.Button
    Friend WithEvents buttonDivide As System.Windows.Forms.Button
    Friend WithEvents buttonDecimal As System.Windows.Forms.Button
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents menuExit As System.Windows.Forms.MenuItem
    Friend WithEvents menuAbout As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblDisplay = New System.Windows.Forms.TextBox
        Me.button1 = New System.Windows.Forms.Button
        Me.button2 = New System.Windows.Forms.Button
        Me.button3 = New System.Windows.Forms.Button
        Me.button4 = New System.Windows.Forms.Button
        Me.button5 = New System.Windows.Forms.Button
        Me.button6 = New System.Windows.Forms.Button
        Me.button7 = New System.Windows.Forms.Button
        Me.button8 = New System.Windows.Forms.Button
        Me.button9 = New System.Windows.Forms.Button
        Me.button0 = New System.Windows.Forms.Button
        Me.buttonEqual = New System.Windows.Forms.Button
        Me.buttonAdd = New System.Windows.Forms.Button
        Me.buttonSubtract = New System.Windows.Forms.Button
        Me.buttonMultiply = New System.Windows.Forms.Button
        Me.buttonDivide = New System.Windows.Forms.Button
        Me.buttonDecimal = New System.Windows.Forms.Button
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.menuExit = New System.Windows.Forms.MenuItem
        Me.menuAbout = New System.Windows.Forms.MenuItem
        Me.SuspendLayout()
        '
        'lblDisplay
        '
        Me.lblDisplay.Location = New System.Drawing.Point(16, 8)
        Me.lblDisplay.Name = "lblDisplay"
        Me.lblDisplay.Size = New System.Drawing.Size(192, 20)
        Me.lblDisplay.TabIndex = 0
        Me.lblDisplay.Text = ""
        Me.lblDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'button1
        '
        Me.button1.Location = New System.Drawing.Point(16, 120)
        Me.button1.Name = "button1"
        Me.button1.Size = New System.Drawing.Size(40, 32)
        Me.button1.TabIndex = 1
        Me.button1.Text = "1"
        '
        'button2
        '
        Me.button2.Location = New System.Drawing.Point(64, 120)
        Me.button2.Name = "button2"
        Me.button2.Size = New System.Drawing.Size(40, 32)
        Me.button2.TabIndex = 2
        Me.button2.Text = "2"
        '
        'button3
        '
        Me.button3.Location = New System.Drawing.Point(112, 120)
        Me.button3.Name = "button3"
        Me.button3.Size = New System.Drawing.Size(40, 32)
        Me.button3.TabIndex = 3
        Me.button3.Text = "3"
        '
        'button4
        '
        Me.button4.Location = New System.Drawing.Point(16, 80)
        Me.button4.Name = "button4"
        Me.button4.Size = New System.Drawing.Size(40, 32)
        Me.button4.TabIndex = 4
        Me.button4.Text = "4"
        '
        'button5
        '
        Me.button5.Location = New System.Drawing.Point(64, 80)
        Me.button5.Name = "button5"
        Me.button5.Size = New System.Drawing.Size(40, 32)
        Me.button5.TabIndex = 5
        Me.button5.Text = "5"
        '
        'button6
        '
        Me.button6.Location = New System.Drawing.Point(112, 80)
        Me.button6.Name = "button6"
        Me.button6.Size = New System.Drawing.Size(40, 32)
        Me.button6.TabIndex = 6
        Me.button6.Text = "6"
        '
        'button7
        '
        Me.button7.Location = New System.Drawing.Point(16, 40)
        Me.button7.Name = "button7"
        Me.button7.Size = New System.Drawing.Size(40, 32)
        Me.button7.TabIndex = 7
        Me.button7.Text = "7"
        '
        'button8
        '
        Me.button8.Location = New System.Drawing.Point(64, 40)
        Me.button8.Name = "button8"
        Me.button8.Size = New System.Drawing.Size(40, 32)
        Me.button8.TabIndex = 8
        Me.button8.Text = "8"
        '
        'button9
        '
        Me.button9.Location = New System.Drawing.Point(112, 40)
        Me.button9.Name = "button9"
        Me.button9.Size = New System.Drawing.Size(40, 32)
        Me.button9.TabIndex = 9
        Me.button9.Text = "9"
        '
        'button0
        '
        Me.button0.Location = New System.Drawing.Point(64, 160)
        Me.button0.Name = "button0"
        Me.button0.Size = New System.Drawing.Size(40, 32)
        Me.button0.TabIndex = 10
        Me.button0.Text = "0"
        '
        'buttonEqual
        '
        Me.buttonEqual.Location = New System.Drawing.Point(112, 160)
        Me.buttonEqual.Name = "buttonEqual"
        Me.buttonEqual.Size = New System.Drawing.Size(40, 32)
        Me.buttonEqual.TabIndex = 11
        Me.buttonEqual.Text = "="
        '
        'buttonAdd
        '
        Me.buttonAdd.Location = New System.Drawing.Point(168, 160)
        Me.buttonAdd.Name = "buttonAdd"
        Me.buttonAdd.Size = New System.Drawing.Size(40, 32)
        Me.buttonAdd.TabIndex = 12
        Me.buttonAdd.Text = "+"
        '
        'buttonSubtract
        '
        Me.buttonSubtract.Location = New System.Drawing.Point(168, 120)
        Me.buttonSubtract.Name = "buttonSubtract"
        Me.buttonSubtract.Size = New System.Drawing.Size(40, 32)
        Me.buttonSubtract.TabIndex = 13
        Me.buttonSubtract.Text = "-"
        '
        'buttonMultiply
        '
        Me.buttonMultiply.Location = New System.Drawing.Point(168, 80)
        Me.buttonMultiply.Name = "buttonMultiply"
        Me.buttonMultiply.Size = New System.Drawing.Size(40, 32)
        Me.buttonMultiply.TabIndex = 14
        Me.buttonMultiply.Text = "*"
        '
        'buttonDivide
        '
        Me.buttonDivide.Location = New System.Drawing.Point(168, 40)
        Me.buttonDivide.Name = "buttonDivide"
        Me.buttonDivide.Size = New System.Drawing.Size(40, 32)
        Me.buttonDivide.TabIndex = 15
        Me.buttonDivide.Text = "/"
        '
        'buttonDecimal
        '
        Me.buttonDecimal.Location = New System.Drawing.Point(16, 160)
        Me.buttonDecimal.Name = "buttonDecimal"
        Me.buttonDecimal.Size = New System.Drawing.Size(40, 32)
        Me.buttonDecimal.TabIndex = 16
        Me.buttonDecimal.Text = "."
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuExit, Me.menuAbout})
        '
        'menuExit
        '
        Me.menuExit.Index = 0
        Me.menuExit.Text = "E&xit"
        '
        'menuAbout
        '
        Me.menuAbout.Index = 1
        Me.menuAbout.Text = "&About"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(224, 206)
        Me.Controls.Add(Me.buttonDecimal)
        Me.Controls.Add(Me.buttonDivide)
        Me.Controls.Add(Me.buttonMultiply)
        Me.Controls.Add(Me.buttonSubtract)
        Me.Controls.Add(Me.buttonAdd)
        Me.Controls.Add(Me.buttonEqual)
        Me.Controls.Add(Me.button0)
        Me.Controls.Add(Me.button9)
        Me.Controls.Add(Me.button8)
        Me.Controls.Add(Me.button7)
        Me.Controls.Add(Me.button6)
        Me.Controls.Add(Me.button5)
        Me.Controls.Add(Me.button4)
        Me.Controls.Add(Me.button3)
        Me.Controls.Add(Me.button2)
        Me.Controls.Add(Me.button1)
        Me.Controls.Add(Me.lblDisplay)
        Me.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calculator"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim fakeCalculator As New ClassLibrary1.Calculator.newCalculator

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Set up everything.
        lblDisplay.Text = fakeCalculator.clearCalc()
        AddHandler button0.Click, AddressOf numClick
        AddHandler button1.Click, AddressOf numClick
        AddHandler button2.Click, AddressOf numClick
        AddHandler button3.Click, AddressOf numClick
        AddHandler button4.Click, AddressOf numClick
        AddHandler button5.Click, AddressOf numClick
        AddHandler button6.Click, AddressOf numClick
        AddHandler button7.Click, AddressOf numClick
        AddHandler button8.Click, AddressOf numClick
        AddHandler button9.Click, AddressOf numClick
        AddHandler buttonDecimal.Click, AddressOf numClick
        AddHandler buttonAdd.Click, AddressOf mathClick
        AddHandler buttonEqual.Click, AddressOf mathClick
        AddHandler buttonSubtract.Click, AddressOf mathClick
        AddHandler buttonMultiply.Click, AddressOf mathClick
        AddHandler buttonDivide.Click, AddressOf mathClick
    End Sub

    Private Sub numClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        lblDisplay.Text = fakeCalculator.addDigit(sender.text)
    End Sub

    Private Sub mathClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        lblDisplay.Text = fakeCalculator.storeAction(sender.text)
    End Sub

    Private Sub menuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuExit.Click
        Application.Exit()
    End Sub

    Private Sub menuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuAbout.Click
        Dim aboutWindow As New about
        aboutWindow.Show()
    End Sub
End Class

Public Class Calculator
    Public Class newCalculator
        Private f_Number As String
        Private s_Number As String
        Private action As String

        Function storeAction(ByVal tempAction As String)
            ' Store the selected math function (addition, substraction, etc).
            '   also responsible for performing said function.
            If action = "" Then
                action = tempAction
                If f_Number <> "" Then
                    s_Number = f_Number
                Else
                    s_Number = "0"
                End If
                f_Number = ""
            Else
                If action = "+" Then
                    s_Number = (Convert.ToDecimal(f_Number) + Convert.ToDecimal(s_Number)).ToString
                ElseIf action = "*" Then
                    s_Number = (Convert.ToDecimal(f_Number) * Convert.ToDecimal(s_Number)).ToString
                ElseIf action = "/" Then
                    s_Number = (Convert.ToDecimal(s_Number) / Convert.ToDecimal(f_Number)).ToString
                ElseIf action = "-" Then
                    s_Number = (Convert.ToDecimal(s_Number) - Convert.ToDecimal(f_Number)).ToString
                End If
                action = tempAction
                f_Number = ""
                Dim randomNumber As New Random(Now.Millisecond)
                Dim tempNumber As Integer = randomNumber.Next(0, 6)
                If tempNumber = 1 Then
                    tempNumber = randomNumber.Next(1, 3)
                    s_Number = (Convert.ToDecimal(s_Number) * Convert.ToDecimal(tempNumber)).ToString
                End If
            End If

            Return s_Number
        End Function

        Function addDigit(ByVal tempDigit As String)
            ' And a number to the end of the string.
            f_Number = f_Number + tempDigit
            If action = "=" Then
                action = ""
                s_Number = "0"
            End If
            Return f_Number
        End Function

        Function clearCalc()
            ' Reset the calculator.
            f_Number = ""
            s_Number = ""
            action = ""
            Return "0"
        End Function
    End Class
End Class

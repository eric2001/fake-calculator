# Fake Calculator
![Fake Calculator](./screenshot.jpg) 

## Description
At first glance, this appears to be a standard 4-function calculator.  However, there's a random number generator in it, which causes it to spit out weird and completely wrong answers every now and then.

**License:** [GPL v.3](http://www.gnu.org/licenses/gpl-3.0.html)

## Instructions
Use it exactly the same way you'd use a regular calculator.

## History
**Version 1.1.0:**
> - Added license agreement (GPL Version 3)
> - Released Source Code
> - Released on 10 October 2007
>
> Download: [Version 1.1.0 Setup](/uploads/8c8cc7c5fdab4a02acfb5193bd33fd01/FakeCalculator110Setup.zip) | [Version 1.1.0 Source](/uploads/f0b72a2b063124d3240700735a4bb669/FakeCalculator110Source.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 29 March 2006
>
> Download: [Version 1.0.0 Setup](/uploads/4056a35362a8f67ca9f1b0a13fd7c199/FakeCalculator100Setup.zip)
